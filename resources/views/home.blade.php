<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tạo domain</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        input {
            border-radius: 5px;
            outline: none;
            padding: 10px 10px;
            border: none;
            border: 1px solid gray;
        }

        button {
            border-radius: 5px;
            outline: none;
            padding: 10px 10px;
            border: 1px solid gray;
            cursor: pointer;
            background-color: #0a0302;
            color: white;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            <form id="form">
                <div style="font-size: 14px">
                    Trỏ tên miền của bạn về:
                    <pre>{{request()->server('SERVER_ADDR')}}</pre>
                </div>
                <input id="domain" name="domain" type="text" placeholder="Nhập tên miền">
                <button type="submit">
                    Tạo SSL
                </button>
                <div id="message" style="font-size: 16px;font-weight: bold"></div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js"></script>
<script>
    const form = document.forms['form'];
    form.addEventListener('submit', async (event) => {
        event.preventDefault();
        const message = document.getElementById('message');
        message.innerHTML = 'Đang tạo kết nối đến máy chủ';
        message.style.color = 'blue';
        try {
            const domain = document.getElementById('domain').value;
            const response = await axios.post('/api/create', {
                domain
            });
            message.innerHTML = response.data.message;
            message.style.color = 'green';
        } catch (e) {
            message.innerHTML = e.response.data.message;
            message.style.color = 'red';
        }

    });

</script>
</body>
</html>
