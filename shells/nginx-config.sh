#!/usr/bin/env bash

domain=$1
nginx=$2
root=$3

# Kiểm tra có biến domain hay không
if [ -z "$domain" ]; then
    echo "Domain not found!"
    exit 0
fi

# Kiểm tra có biến nginx hay không
if [ -z "$nginx" ]; then
    echo "Nginx path not found!"
    exit 0
fi

#if [ -d /etc/nginx/sites-available/$domain.conf ]
if [ -e /etc/nginx/sites-available/$domain ]
then
    echo "Domain ssl $domain exits"
    exit 0
else
#    sed "s/{domain}/$domain/gi" $root/{domain}.conf.stub > $nginx/$domain.conf
# Tạo file domain mới từ file template
    sed "s/{domain}/$domain/gi" $root/{domain}.conf.stub > /etc/nginx/sites-available/$domain
# Kiểm tra nginx config và chép config ssl
    nginx -t && sudo ln -s /etc/nginx/sites-available/$domain /etc/nginx/sites-enabled/
#     Reload lại nginx server
    service nginx reload
    exit 1
fi
