<?php


Route::get('/.well-known/acme-challenge/{token}', [\Azibai\Certificate\Http\Controllers\HomeController::class, 'challenge']);

Route::domain('{domain}')->group(function () {
    Route::get('/', [\Azibai\Certificate\Http\Controllers\HomeController::class, 'local'])->name('azibai.local');
});

Route::domain('{domain}.{al}')->group(function () {
    Route::get('/', [\Azibai\Certificate\Http\Controllers\HomeController::class, 'domain'])->name('azibai.domain');
});

Route::domain('{subdomain}.{domain}.{al}')->group(function () {
    Route::get('/', [\Azibai\Certificate\Http\Controllers\HomeController::class, 'subdomain'])->name('azibai.subdomain');
});

Route::get('/create', [\Azibai\Certificate\Http\Controllers\HomeController::class, 'create'])->name('azibai.create');
