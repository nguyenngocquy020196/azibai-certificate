<?php

use Azibai\Certificate\Http\Middleware\APIKeyAccessMiddleware;
use Azibai\Certificate\Http\Controllers\HomeController;

Route::get('/dns', [HomeController::class, 'dns']);
Route::post('/create', [HomeController::class, 'store'])->middleware(APIKeyAccessMiddleware::class)->name('azibai-store');
Route::get('/test', [HomeController::class, 'test']);
Route::get('/openssl', function () {
    return \OpenSsl::create([
        "countryName" => "GB",
        "stateOrProvinceName" => "Somerset",
        "localityName" => "Glastonbury",
        "organizationName" => "The Brain Room Limited",
        "organizationalUnitName" => "PHP Documentation Team",
        "commonName" => "Wez Furlong",
        "emailAddress" => "wez@example.com"
    ]);
});
