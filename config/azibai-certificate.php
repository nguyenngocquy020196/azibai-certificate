<?php

use Azibai\Certificate\Supports\DefaultPathGenerator;

$api = "https://acme-staging-v02.api.letsencrypt.org/directory";
$api_production = "https://acme-v02.api.letsencrypt.org/directory";

return [
    'email' => env('AZIBAI_CERTIFICATE_EMAIL', 'contact@ngocquy.net'),
    'lets_encrypt_api_url' => env('AZIBAI_LETS_ENCRYPT_API_URL', $api),
    'path_generator' => DefaultPathGenerator::class,
    'nginx_path' => '/Users/nguyenquy',
    'api_key' => env('AZIBAI_API_KEY', '4297f44b13955235245b2497399d7a93')
];
