<?php

namespace Azibai\Certificate\Interfaces;

interface PathGenerator
{
    public function getChallengePath(string $token): string;

    public function getCertificatePath(string $domain, string $filename): string;
}
