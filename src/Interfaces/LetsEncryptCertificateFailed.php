<?php

namespace Azibai\Certificate\Interfaces;

interface LetsEncryptCertificateFailed
{
    public function getException(): \Throwable;
}
