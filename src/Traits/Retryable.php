<?php

namespace Azibai\Certificate\Traits;

trait Retryable
{
    public $tries;

    public $retryAfter;

    public $retryList;

    public function retryAfter(): int
    {
        return (!empty($this->retryList)) ? $this->retryList[$this->attempts() - 1] : 0;
    }
}
