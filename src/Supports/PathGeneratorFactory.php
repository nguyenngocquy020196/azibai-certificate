<?php

namespace Azibai\Certificate\Supports;

use Azibai\Certificate\Exceptions\InvalidPathGenerator;
use Azibai\Certificate\Interfaces\PathGenerator;

class PathGeneratorFactory
{
    public static function create(): PathGenerator
    {
        $class = config('azibai-certificate.path_generator');

        throw_if($class === null, new InvalidPathGenerator('null'));

        throw_if(!class_exists($class), new InvalidPathGenerator($class));

        return app($class);
    }
}
