<?php

namespace Azibai\Certificate\Supports;

use Azibai\Certificate\Interfaces\PathGenerator;

class DefaultPathGenerator implements PathGenerator
{
    public function getChallengePath(string $token): string
    {
        return ".well-known/acme-challenge/$token";
    }

    public function getCertificatePath(string $domain, string $filename): string
    {
        return "lets-encrypt/certificates/$domain/$filename";
    }
}
