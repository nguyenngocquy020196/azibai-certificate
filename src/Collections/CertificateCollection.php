<?php

namespace Azibai\Certificate\Collections;

use Azibai\Certificate\Models\CertificateModel;
use Illuminate\Support\Collection;

class CertificateCollection extends Collection
{
    public function renew(): self
    {
        $this->each(function (CertificateModel $certificate): void {
            $certificate->renew();
        });
        return $this;
    }

    public function renewNow(): self
    {
        $this->each(function (CertificateModel $certificate): void {
            $certificate->renewNow();
        });
        return $this;
    }
}
