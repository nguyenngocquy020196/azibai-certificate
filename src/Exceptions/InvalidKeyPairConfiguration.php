<?php

namespace Azibai\Certificate\Exceptions;

use Exception;

class InvalidKeyPairConfiguration extends Exception
{
    public function __construct($message)
    {
        $message .= '\nConfiguration:\nPublic key path: ' .
            storage_path('app/lets-encrypt/keys/azibai.pub.pem')
            . '\nPrivate key path: '
            . storage_path('app/lets-encrypt/keys/azibai.pem');

        parent::__construct($message);
    }
}
