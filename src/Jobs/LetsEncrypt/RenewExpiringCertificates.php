<?php

namespace Azibai\Certificate\Jobs\LetsEncrypt;

use Azibai\Certificate\Collections\CertificateCollection;
use Azibai\Certificate\Events\LetsEncrypt\RenewExpiringCertificatesFailed;
use Azibai\Certificate\Models\CertificateModel;
use Azibai\Certificate\Traits\Retryable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RenewExpiringCertificates implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue, SerializesModels, Retryable;

    public function __construct(int $tries = null, int $retryAfter = null, $retryList = [])
    {
        $this->tries = $tries;
        $this->retryAfter = $retryAfter;
        $this->retryList = $retryList;
    }

    public function handle()
    {
        CertificateModel::query()
            ->requiresRenewal()
            ->chunk(100, function (CertificateCollection $certificates) {
                $certificates->renew();
            });
    }

    public function failed(\Throwable $exception)
    {
        event(new RenewExpiringCertificatesFailed($exception));
    }
}
