<?php

namespace Azibai\Certificate\Jobs\LetsEncrypt;

use Azibai\Certificate\Traits\Retryable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LetsEncrypt;

class RegisterAccount implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue, SerializesModels, Retryable;

    protected $email;

    public function __construct(string $email = null, int $tries = null, int $retryAfter = null, $retryList = [])
    {
        $this->email = $email;
        $this->tries = $tries;
        $this->retryAfter = $retryAfter;
        $this->retryList = $retryList;
    }

    public function handle()
    {
        $client = LetsEncrypt::createClient();
        $client->registerAccount($this->email, null);
    }

    public function fail(\Throwable $exception)
    {
        event(new RegisterAccountFailed($exception));
    }
}
