<?php

namespace Azibai\Certificate\Jobs\LetsEncrypt;

use AcmePhp\Core\Protocol\AuthorizationChallenge;
use Azibai\Certificate\Events\LetsEncrypt\CleanUpChallengeFailed;
use Azibai\Certificate\Supports\PathGeneratorFactory;
use Azibai\Certificate\Traits\Retryable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class CleanUpChallenge implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue, SerializesModels, Retryable;

    protected $challenge;

    protected $client;

    public function __construct(AuthorizationChallenge $httpChallenge, int $tries = null, int $retryAfter = null, array $retryList = [])
    {
        $this->challenge = $httpChallenge;
        $this->tries = $tries;
        $this->retryAfter = $retryAfter;
        $this->retryList = $retryList;
    }

    public function handle()
    {
        $generator = PathGeneratorFactory::create();
        Storage::disk('public')->delete($generator->getChallengePath($this->challenge->getToken()));
    }

    public function failed(\Throwable $exception)
    {
        event(new CleanUpChallengeFailed($exception));
    }
}
