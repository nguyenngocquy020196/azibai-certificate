<?php

namespace Azibai\Certificate\Jobs\LetsEncrypt;

use AcmePhp\Core\Protocol\AuthorizationChallenge;
use Azibai\Certificate\Events\LetsEncrypt\ChallengeAuthorizationFailed;
use Azibai\Certificate\Traits\Retryable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use LetsEncrypt;

class ChallengeAuthorization implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue, SerializesModels, Retryable;

    protected $challenge;

    public function __construct(AuthorizationChallenge $httpChallenge, int $tries = null, int $retryAfter = null, array $retryList = [])
    {
        $this->challenge = $httpChallenge;
        $this->tries = $tries;
        $this->retryAfter = $retryAfter;
        $this->retryList = $retryList;
    }

    public function handle()
    {
        $client = LetsEncrypt::createClient();
        $client->challengeAuthorization($this->challenge);
        CleanUpChallenge::dispatch($this->challenge, $this->tries, $this->retryAfter, $this->retryList);
    }


    public function failed(\Throwable $exception)
    {
        event(new ChallengeAuthorizationFailed($exception));
    }
}
