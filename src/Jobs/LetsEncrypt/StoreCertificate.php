<?php

namespace Azibai\Certificate\Jobs\LetsEncrypt;

use AcmePhp\Ssl\Certificate;
use AcmePhp\Ssl\PrivateKey;
use Azibai\Certificate\Encoders\PemEncoder;
use Azibai\Certificate\Events\LetsEncrypt\StoreCertificateFailed;
use Azibai\Certificate\Exceptions\FailedToStoreCertificate;
use Azibai\Certificate\Interfaces\PathGenerator;
use Azibai\Certificate\Models\CertificateModel;
use Azibai\Certificate\Supports\PathGeneratorFactory;
use Azibai\Certificate\Traits\Retryable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class StoreCertificate implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue, SerializesModels, Retryable;

    protected $certificate;

    protected $dbCertificate;

    protected $privateKey;


    public function __construct(CertificateModel $dbCertificate, Certificate $certificate, PrivateKey $privateKey, int $tries = null, int $retryAfter = null, $retryList = [])
    {
        $this->dbCertificate = $dbCertificate;
        $this->certificate = $certificate;
        $this->privateKey = $privateKey;
        $this->tries = $tries;
        $this->retryAfter = $retryAfter;
        $this->retryList = $retryList;
    }

    public function handle()
    {
        $certPem = PemEncoder::encode($this->certificate->getPEM());
        $chainPem = collect($this->certificate->getIssuerChain())
            ->reduce(function (string $carry, Certificate $certificate): string {
                return $carry . PemEncoder::encode($certificate->getPEM());
            }, '');

        $fullChainPem = $certPem . $chainPem;

        $privkeyPem = PemEncoder::encode($this->privateKey->getPEM());

        $factory = PathGeneratorFactory::create();

        $this->storeInPossiblyNonExistingDirectory($factory, 'cert', $certPem);
        $this->storeInPossiblyNonExistingDirectory($factory, 'chain', $certPem);
        $this->storeInPossiblyNonExistingDirectory($factory, 'full_chain', $fullChainPem);
        $this->storeInPossiblyNonExistingDirectory($factory, 'private_key', $privkeyPem);
        $this->dbCertificate->last_renewed_at = now();
        $this->dbCertificate->created = true;
        $this->dbCertificate->save();
        Artisan::call("certificate:nginx", [
            '-d' => $this->dbCertificate->domain
        ]);
    }

    protected function storeInPossiblyNonExistingDirectory(PathGenerator $generator, string $filename, string $contents): void
    {
        $path = $generator->getCertificatePath($this->dbCertificate->domain, $filename . '.pem');
        $directory = File::dirname($path);
        $fs = Storage::disk();
        if (!$fs->exists($directory)) {
            $fs->makeDirectory($directory);
        }

        $this->dbCertificate[$filename . '_path'] = $path;

        if ($fs->put($path, $contents) === false) {
            throw new FailedToStoreCertificate($path);
        }
    }

    public function failed(\Throwable $exception)
    {
        event(new StoreCertificateFailed($exception, $this->dbCertificate));
    }
}
