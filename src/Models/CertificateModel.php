<?php

namespace Azibai\Certificate\Models;

use Azibai\Certificate\Builders\CertificateBuilder;
use Azibai\Certificate\Collections\CertificateCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LetsEncrypt;

class CertificateModel extends Model
{
    use SoftDeletes;

    protected $table = 'azibai_certificates';

    protected $guarded = ['id'];

    protected $dates = ['last_renewed_at'];

    protected $casts = [
        'created' => 'boolean',
    ];

    public function newEloquentBuilder($query): CertificateBuilder
    {
        return new CertificateBuilder($query);
    }

    public function newCollection(array $models = [])
    {
        return new CertificateCollection($models);
    }

    public function getHasExpiredAttribute(): bool
    {
        return $this->last_renewed_at && $this->last_renewed_at->diffInDays(now()) >= 90;
    }

    public function renew()
    {
        return LetsEncrypt::renew($this);
    }

    public function renewNow(): self
    {
        return LetsEncrypt::renewNow($this);
    }
}
