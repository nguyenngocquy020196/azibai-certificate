<?php

namespace Azibai\Certificate;

use AcmePhp\Core\Http\SecureHttpClientFactory;
use AcmePhp\Ssl\Generator\KeyPairGenerator;
use AcmePhp\Ssl\KeyPair;
use AcmePhp\Ssl\PrivateKey;
use AcmePhp\Ssl\PublicKey;
use Azibai\Certificate\Exceptions\InvalidKeyPairConfiguration;
use Azibai\Certificate\Jobs\LetsEncrypt\RegisterAccount;
use Azibai\Certificate\Jobs\LetsEncrypt\RequestAuthorization;
use Azibai\Certificate\Jobs\LetsEncrypt\RequestCertificate;
use Azibai\Certificate\Models\CertificateModel;
use AcmePhp\Core\AcmeClient;
use Illuminate\Support\Facades\File;

class LetsEncrypt
{

    protected $factory;

    private static $instance;

    public function __construct(SecureHttpClientFactory $factory)
    {
        $this->factory = $factory;
        self::$instance = $this;
    }

    /**
     * Tạo chứng chỉ ssl mới thông qua hàng đợi
     * @param string $domain
     * @param array $chain
     * @return array
     */
    public function create(string $domain, array $chain = []): array
    {
        $certificate = CertificateModel::create([
            'domain' => $domain,
        ]);
        $email = config('azibai-certificate.email');
        return [
            $certificate,
            RegisterAccount::withChain(
                array_merge([
                    new RequestAuthorization($certificate),
                    new RequestCertificate($certificate),
                ], $chain))->dispatch($email)
        ];
    }

    /**
     * Tạo chứng chỉ ssl mới không thông qua hàng đợi
     * @param string $domain
     * @return mixed
     */
    public function createNow(string $domain)
    {
        $email = config('azibai-certificate.email');
        $certificate = CertificateModel::create([
            'domain' => $domain,
        ]);
        RegisterAccount::dispatchNow($email);
        RequestAuthorization::dispatchNow($certificate);
        RequestCertificate::dispatchNow($certificate);
        return $certificate->refresh();
    }

    /**
     * Tạo kết nối đến server của letsencrypt
     */
    public function createClient(): AcmeClient
    {
        $keyPair = $this->getKeyPair();
        $secureHttpClient = self::$instance->factory->createSecureHttpClient($keyPair);

        return new AcmeClient(
            $secureHttpClient,
            config('azibai-certificate.lets_encrypt_api_url')
        );
    }

    public function getKeyPair()
    {
        /**
         * Lấy thư mục chứa thông tin đăng nhập
         */
        $publicKeyPath = storage_path("app/lets-encrypt/keys/azibai.pub.pem");
        $privateKeyPath = storage_path("app/lets-encrypt/keys/azibai.pem");

        /**
         * Nếu file chưa tồn tại thì tạo key tài khoản mới
         */
        if (!file_exists($privateKeyPath) && !file_exists($publicKeyPath)) {
            $keyPairGenerator = new KeyPairGenerator();
            $keyPair = $keyPairGenerator->generateKeyPair();
            File::ensureDirectoryExists(File::dirname($publicKeyPath));
            File::ensureDirectoryExists(File::dirname($privateKeyPath));
            file_put_contents($publicKeyPath, $keyPair->getPublicKey()->getPEM());
            file_put_contents($privateKeyPath, $keyPair->getPrivateKey()->getPEM());
            return $keyPair;
        }

        if (! file_exists($privateKeyPath)) {
            throw new InvalidKeyPairConfiguration('Private key does not exist but public key does.');
        }

        if (! file_exists($publicKeyPath)) {
            throw new InvalidKeyPairConfiguration('Public key does not exist but private key does.');
        }

        $publicKey = new PublicKey(file_get_contents($publicKeyPath));
        $privateKey = new PrivateKey(file_get_contents($privateKeyPath));

        return new KeyPair($publicKey, $privateKey);
    }
}
