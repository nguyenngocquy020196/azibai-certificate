<?php

namespace Azibai\Certificate;

class OpenSsl
{
    public function create(array $data = [], $bit = 2048)
    {
        $configs = [
            'private_key_bits' => $bit,
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
            "digest_alg" => "sha256",
        ];
        $privateKey = openssl_pkey_new($configs);
        openssl_pkey_export($privateKey, $out);
        $csr = openssl_csr_new($data, $privateKey, $configs);
        openssl_csr_export($csr, $csr_out);
        return $csr_out;
    }
}
