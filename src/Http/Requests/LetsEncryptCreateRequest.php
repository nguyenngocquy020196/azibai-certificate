<?php

namespace Azibai\Certificate\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LetsEncryptCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'domain' => 'required|string'
        ];
    }
}
