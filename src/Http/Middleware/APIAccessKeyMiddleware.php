<?php

namespace Azibai\Certificate\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class APIAccessKeyMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $conditions = $request->hasHeader('azibai-api-key') && $request->header('azibai-api-key') === config('azibai-certificate.api_key');
        if (!$conditions) {
            abort(403,'Access denied !');
        }
        return $next($request);
    }
}
