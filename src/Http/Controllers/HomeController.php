<?php

namespace Azibai\Certificate\Http\Controllers;

use Azibai\Certificate\Http\Requests\LetsEncryptCreateRequest;
use Azibai\Certificate\Http\Responses\BaseHttpResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use LetsEncrypt;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Storage;

class HomeController extends Controller
{
    use DispatchesJobs, ValidatesRequests;

    public function local($domain)
    {
        $url = $domain;
        return view('azibai-certificate::welcome', compact('url'));
    }

    public function domain($domain, $al)
    {
        $url = "$domain.$al";
        return view('azibai-certificate::welcome', compact('url'));
    }

    public function subdomain($subdomain, $domain, $al)
    {
        $url = "$subdomain.$domain.$al";
        return view('azibai-certificate::welcome', compact('url'));
    }

    public function dns(Request $request, BaseHttpResponse $response): BaseHttpResponse
    {
        if ($request->has('domain')) {
            $dns = gethostbyname($request->get('domain'));
            return $response->setData(['dns' => $dns]);
        }
        return $response->setData([]);
    }

    public function create()
    {
        return view('azibai-certificate::home');
    }

    public function store(LetsEncryptCreateRequest $request, BaseHttpResponse $response): BaseHttpResponse
    {
        $domain = $request->input('domain');
        $dns = gethostbyname($domain);
        if ($dns !== request()->server('SERVER_ADDR')) {
            return $response->setError(true)->setCode(422)->setMessage('Bạn chưa trỏ domain về hệ thống !');
        }
        LetsEncrypt::create($domain);
        return $response->setMessage('Domain của bạn đang được chờ trong hàng đợi !');
    }

    public function challenge($token)

    {

        try {

            return Storage::get('.well-known/acme-challenge/' . $token);

        } catch (\Exception $exception) {

            abort(404);
        }
    }

    public function test(Request $request, BaseHttpResponse $response): BaseHttpResponse
    {
        if ($request->has('domain')) {
            $domain = $request->get('domain');
//            $base = base_path();
//            $cmd = "php $base/artisan certificate:nginx -d $domain";
//            shell_exec($cmd);
            Artisan::call("certificate:nginx", [
                '-d' => $domain
            ]);
            return $response->setMessage(['message' => 'Dang tao']);
        }
        return $response->setCode(403)->setMessage(['message' => 'nhap domain']);
    }
}
