<?php

namespace Azibai\Certificate\Facades;

use Azibai\Certificate\PaVietNam;
use Illuminate\Support\Facades\Facade;

class PaVietNamFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return PaVietNam::class;
    }
}
