<?php

namespace Azibai\Certificate\Facades;

use Azibai\Certificate\OpenSsl;
use Illuminate\Support\Facades\Facade;

class OpenSslFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return OpenSsl::class;
    }
}
