<?php

namespace Azibai\Certificate\Facades;

use Azibai\Certificate\LetsEncrypt;
use Illuminate\Support\Facades\Facade;

class LetsEncryptFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return LetsEncrypt::class;
    }
}
