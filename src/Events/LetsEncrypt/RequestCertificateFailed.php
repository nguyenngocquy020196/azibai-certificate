<?php

namespace Azibai\Certificate\Events\LetsEncrypt;

use Azibai\Certificate\Interfaces\LetsEncryptCertificateFailed;
use Azibai\Certificate\Models\CertificateModel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RequestCertificateFailed implements LetsEncryptCertificateFailed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $exception;
    protected $certificate;

    public function __construct(\Throwable $exception, CertificateModel $certificate)
    {
        $this->exception = $exception;
        $this->certificate = $certificate;
    }

    public function getException(): \Throwable
    {
        return $this->exception;
    }

    public function getCertificate(): CertificateModel
    {
        return $this->certificate;
    }
}
