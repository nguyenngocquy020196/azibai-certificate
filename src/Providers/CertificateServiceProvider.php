<?php

namespace Azibai\Certificate\Providers;

use AcmePhp\Core\Http\Base64SafeEncoder;
use AcmePhp\Core\Http\SecureHttpClientFactory;
use AcmePhp\Core\Http\ServerErrorHandler;
use AcmePhp\Ssl\Parser\KeyParser;
use AcmePhp\Ssl\Signer\DataSigner;
use Azibai\Certificate\Commands\ConfigNginxCommand;
use Azibai\Certificate\Commands\LetsEncryptCommand;
use Azibai\Certificate\Commands\PaVietNamCommand;
use Azibai\Certificate\Facades\LetsEncryptFacade;
use Azibai\Certificate\Facades\OpenSslFacade;
use Azibai\Certificate\Facades\PaVietNamFacade;
use Azibai\Certificate\Http\Middleware\ForceJsonResponseMiddleware;
use Azibai\Certificate\LetsEncrypt;
use Azibai\Certificate\PaVietNam;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client as GuzzleHttpClient;

class CertificateServiceProvider extends ServiceProvider
{
    protected $namespace = 'Azibai\Certificate\Http\Controllers';

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/azibai-certificate.php', 'azibai-certificate');
        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('LetsEncrypt', LetsEncryptFacade::class);
            $loader->alias('OpenSsl', OpenSslFacade::class);
            $loader->alias('PaVietNam', PaVietNamFacade::class);
        });

        $this->app->bind(LetsEncrypt::class, function () {
            return new LetsEncrypt(
                new SecureHttpClientFactory(
                    new GuzzleHttpClient(),
                    new Base64SafeEncoder(),
                    new KeyParser(),
                    new DataSigner(),
                    new ServerErrorHandler()
                )
            );
        });
        $this->app->bind(PaVietNam::class, function () {
            return new PaVietNam();
        });
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../../config/azibai-certificate.php' => config_path('azibai-certificate.php'),
            ], 'azibai-certificate');

            $migrationFileName = 'create_azibai_certificates_table.php';
            if (!$this->migrationFileExists($migrationFileName)) {
                $this->publishes([
                    __DIR__ . "/../../database/migrations/{$migrationFileName}.stub" => database_path('migrations/' . date('Y_m_d_His', time()) . '_' . $migrationFileName),
                ], 'azibai-certificate');
            }
        }
        $this->commands([
            LetsEncryptCommand::class,
            PaVietNamCommand::class,
            ConfigNginxCommand::class
        ]);
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'azibai-certificate');
        Route::prefix('api')
            ->middleware('api')
            ->middleware(ForceJsonResponseMiddleware::class)
            ->namespace($this->namespace)
            ->group(__DIR__ . '/../../routes/api.php');
    }

    public static function migrationFileExists(string $migrationFileName): bool
    {
        $len = strlen($migrationFileName);
        foreach (glob(database_path("migrations/*.php")) as $filename) {
            if ((substr($filename, -$len) === $migrationFileName)) {
                return true;
            }
        }
        return false;
    }

}
