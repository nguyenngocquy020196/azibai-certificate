<?php

namespace Azibai\Certificate\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ConfigNginxCommand extends Command
{
    protected $name = 'certificate:nginx';

    protected $description = 'create nginx config ssl for domain';

    public function handle()
    {
        if (!$domain = $this->option('domain')) {
            $domain = $this->ask('For which domain do you want to create an SSL certificate?');
        }
        rescue(function () use ($domain) {
            $nginx = config('azibai-certificate.nginx_path');
            $shell_path = __DIR__ . '/../../shells';
            $output = shell_exec("sh $shell_path/nginx-config.sh $domain $nginx $shell_path");
            $this->comment($output);
            $this->comment("SSL config success");
            $this->comment($shell_path);
        }, function (Throwable $e) use ($domain) {
            $this->error('Failed to generate a certificate for ' . $domain);
            $this->error($e->getMessage());
        }, false);
    }

    public function getOptions(): array
    {
        return [
            ['domain', 'd', InputOption::VALUE_OPTIONAL, 'Generate a certificate for the given domain name.'],
        ];
    }
}
