<?php

namespace Azibai\Certificate\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class PaVietNamCommand extends Command
{
    protected $name = 'certificate:pa';

    protected $description = 'Creates an SSL certificate through PAVietNam.';

    public function handle()
    {

    }

    public function getOptions(): array
    {
        return [
            ['domain', 'd', InputOption::VALUE_OPTIONAL, 'Generate a certificate for the given domain name(s). Multiple domains can be separated by a comma.'],
        ];
    }
}
