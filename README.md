### Create modules/azibai

pull certificate

add composer

```
"require": {
    "azibai/certificate":"*@dev"
},
"repositories": [
    {
        "type": "path",
        "url": "./modules/azibai/certificate"
    }
]
```

```
composer install
```

```
php artisan vendor:publish --provider="Azibai\Certificate\Providers\CertificateServiceProvider" --tag="azibai-certificate"
```

```
php artisan migrate
```

Run job batch

```bash
php artisan queue:work
```

nginx default set allow all domain

```
server_name _;
```

create new ssl and nginx config

```cmd
php artisan certificate:free -d mydomain.com
```

use

```php
# Job
LetsEncrypt::create('mydomain.com); 

# now
LetsEncrypt::createNow('mydomain.com);

```

renew certificate 
```php
protected function schedule(Schedule $schedule)
{
    $schedule->job(new \Azibai\Certificate\Jobs\LetsEncrypt\RenewExpiringCertificates)->daily();
}
```
